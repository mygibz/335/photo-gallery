import { Component } from '@angular/core';
import { TabsPage } from '../tabs/tabs.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.css']
})
export class Tab1Page {

  constructor() {}

  async darkModeTogglePressed() {
    localStorage.setItem('dark', (localStorage.getItem('dark') == "false").toString());
    (new TabsPage()).changeHeaderColor(localStorage.getItem('dark'));
  }
}
