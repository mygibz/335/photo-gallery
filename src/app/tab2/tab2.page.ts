import { Component } from '@angular/core';
import { PhotoService } from '../services/photo.service';
import { ActionSheetController } from '@ionic/angular';
import { TabsPage } from '../tabs/tabs.page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.css']
})
export class Tab2Page {

  constructor(
    public photoService: PhotoService, 
    public actionSheetController: ActionSheetController
  ) {}


  ngOnInit() {
    this.photoService.loadSaved();
  }

  async darkModeTogglePressed() {
    localStorage.setItem('dark', (localStorage.getItem('dark') == "false").toString());
    (new TabsPage()).changeHeaderColor(localStorage.getItem('dark'));
  }

  public async showActionSheet(photo, position, e) {
    (new TabsPage()).changeHeaderColor(localStorage.getItem('dark'));

    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Open',
        icon: 'open',
        role: 'open',
        handler: () => {
            
          var modal = document.createElement("div");
          var image = document.createElement("img");
          var div = document.getElementById('content');
          modal.id = "modal";
          modal.appendChild(image);
          modal.onclick = function () {
            document.getElementById(modal.id).remove();
          };

          modal.style.zIndex = "10";
          modal.style.position = "fixed";
          modal.style.top = "0";
          modal.style.left = "0";
          modal.style.width = "100%";
          modal.style.height = "100%";
          modal.style.backgroundColor = "rgba(0, 0, 0, 0.45)";

          image.style.position = "absolute";
          image.style.top = "50%";
          image.style.left = "50%";
          image.style.transform = "translate(-50%, -50%)";
          image.style.borderRadius = "15px";
          image.style.maxHeight = "90vh";
          image.style.maxWidth = "90vw";
          image.style.objectFit = "contain";


          image.style.width = "90vw";

          image.src = e.target.src;
          div.appendChild(modal)

          }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
          }
      }]
    });
    await actionSheet.present();
  }
}
