import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.css']
})
export class TabsPage {

  constructor(private statusBar: StatusBar = new StatusBar()) {}

  ngOnInit() {
    this.changeHeaderColor();
    localStorage.setItem('dark', 'false')
  }

  public async changeHeaderColor(status = localStorage.getItem('dark')) {
    await new Promise( resolve => setTimeout(resolve, 50) );

    if (status == "true") {
      
      this.statusBar.styleBlackOpaque();
      this.statusBar.backgroundColorByHexString('#30373B');

      document.querySelectorAll("ion-card, ion-content, ion-fab-button, ion-tab-bar, ion-header, ion-action-sheet").forEach(element => {
          element.classList.add("dark");
      });
      document.querySelectorAll("ion-action-sheet").forEach(element => {
          element.classList.add("dark");
          // should be taken care of by css but doesn't work
          element.style.setProperty("--background", "#30373B");
          element.style.setProperty("--color", "white");
      });
      document.querySelectorAll("button").forEach(element => {
        // idk why, but it's necesairy
        element.style.setProperty("color", "white"); // idk why it says that it's wrong, but it works
      });
    }
    else {
      
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#ffffff');

      document.querySelectorAll(".dark").forEach(element => {
        element.classList.remove("dark");
      });
      document.querySelectorAll("ion-action-sheet").forEach(element => {
          // should be taken care of by css but doesn't work
          element.style.setProperty("--background", "");
          element.style.setProperty("--color", "");
      });
      document.querySelectorAll("button").forEach(element => {
        // idk why, but it's necesairy
        element.style.setProperty("color", ""); // idk why it says that it's wrong, but it works
      });
    }

    document.querySelectorAll(".darkModeToggle").forEach(element => {
      (element as HTMLInputElement).checked = (localStorage.getItem('dark') == "true")
    });
  }
}
